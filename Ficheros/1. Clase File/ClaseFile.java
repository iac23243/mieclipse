package e_ClaseFile;

import java.io.File;
import java.io.FilenameFilter;

/*El programa  va a analizar si el directorio en cuestión existe, y se listarán los ficheros y directorios que contiene. 
 * Mostrará también si se tiene permiso de lectura y escritura 
 * 
 */

public class ClaseFile{
	public static void main(String[] args){
		
		File dir = new File("src//e_ClaseFile//prueba"); 
		if (dir.exists()) {
			System.out.println("Existe el directorio "+dir.getName());
			
			File[] ficheros = dir.listFiles();
			System.out.println("\nListamos los ficheros que hay dentro de dicho directorio:\n");
			
			for (File f : ficheros)
				System.out.println(f.getName());
		}
		else{
			System.out.println("El directorio no existe");
		}
		
		if (dir.canRead())
			System.out.println("\nEl directorio existe y tiene permiso de lectura");
		if (dir.canWrite())
			System.out.println("El directorio existe y tiene permiso de escritura"); 

		System.out.println("\nVeamos otros metodos de la clase File:\n");

		//creamos un objeto File con el fichero datos.txt y realizamos pruebas con él
		File archivo =new File ("src//e_ClaseFile//prueba//datos.txt");
		File archivo2 =new File ("src//e_ClaseFile//prueba//datos2.txt");

		System.out.println("datos.txt, ¿Existe?: "+archivo.exists());
		System.out.println("datos.txt, ¿Es un fichero?: "+archivo.isFile());
		
		System.out.println("mostramos la ruta del fichero: "+archivo.getPath());
		System.out.println("mostramos la ruta absoluta del fichero: "+archivo.getAbsolutePath());
		System.out.println("el nombre del fichero es: "+archivo.getName());
		System.out.println("El directorio padre del fichero es: "+archivo.getParent());
		System.out.println("El Tamaño de fichero es: "+archivo.length()+" bytes");
		System.out.println("\n¿el Objeto \"archivo\" es igual que el objeto \"archivo2\"?"+archivo.equals(archivo2));

		System.out.println("Vamos a crear un directorio nuevo,pregunando antes si existe");		
		
		String path1="src//e_ClaseFile//prueba//subdir";  
 
	    File file1=new File(path1);  
	    	if(!file1.exists()){  
	    		file1.mkdir();  
	    		System.out.println("Directorio creado");
	        }  		
	    System.out.println("Array de objetos String con los nombres de los ficheros/directorios del directorio al que apunta el objeto File");
	    	
	    String[] files = dir.list();
	    for (int i = 0; i < files.length; i++) {
            System.out.println(files[i]);
        }
	    
	    
	    System.out.println("Array de objetos String con los nombres de los ficheros/directorios del directorio al que apunta el objeto File"
	    		+ " filtrados");

	    
	    /* 
	     * Instanciamos un objeto que implementa  FilenameFilter. 
	     * Es una interface que filtra nombres de ficheros. Contiene el metodo accept
	     * */
        FilenameFilter filtro = new FilenameFilter() {

            public boolean accept(File f, String name)
            {
                return name.startsWith("d");
            }
        };
        
        String[] files2 = dir.list(filtro);

        System.out.println("Los fichero son:");
        for (String f : files2)
            System.out.println(f);
        
/*		//mismo for que el anterior pero realizado de forma clasica
	    for (int i = 0; i < files2.length; i++) {
            System.out.println(files2[i]);
        }
 */ 			

		//creamos otro objeto fichero
		File dest =new File ("src//e_ClaseFile//prueba//info.txt");


		System.out.println("\n¿Se ha renombrado el fichero?"+archivo.renameTo(dest));
		System.out.println("el nombre del fichero es: "+dest.getName());

		//volvemos s renombrar para tenerlo igual que al principio 
		System.out.println("¿Se ha renombrado el fichero?"+dest.renameTo(archivo));

		//para borrar el fichero
		//archivo.delete();

		
	}
}