package ej2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *Clase hecha para manejar ficheros con facilidad, hay metodos para crear y leer ficheros de texto 
 * 
 *@version 1.0
 * @author usuario
 */

public class GestionFicheros {
	/**
	 * 
	 * metodo para crear una carpeta en el paquete de la clase que lo implemente
	 */
	
	public static void crearCarpeta(String nombreCarpeta) {
		File carpeta = new File ("src//ej2//"+nombreCarpeta);
		
		if(!carpeta.exists()) {
			carpeta.mkdir();
			System.out.println("Carpeta creada");
		}else {
			System.out.println("La carpeta ya existe");
		}
	}//fin crear carpeta
	
	
	
	public static void leerArchivo(String ruta) throws IOException {
		File f = new File(ruta);
		
		BufferedReader lector = new BufferedReader (new FileReader(f));
		String linea;
		
		linea=lector.readLine();
		
		while(linea!=null) {
			
			System.out.println(linea);
			linea=lector.readLine();
			
		}
		lector.close();
	}
}
