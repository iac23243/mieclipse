package ej2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class ej2 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		BufferedReader entrada = new BufferedReader (new InputStreamReader (System.in));
		int [] nums = new int [100];
		int cont=0;
		int menu=0,salida=0;
		
		for(int i=0;i<200;i++) {
			
			if(i%2==0) {
				nums[cont]=i;
				cont++;
			}	
			
		}
		
		System.out.println("Escribe el nombre de la nueva carpeta");
		String nombreCarpeta=entrada.readLine();
		
		File carpeta = new File("src//ej2//"+nombreCarpeta);
		
		if(!carpeta.exists()) {
			carpeta.mkdir();
			System.out.println("Carpeta creada");
		}else {
			System.out.println("La carpeta indicada ya existia, se usará");
		}
		
		
		System.out.println("Escribe el nombre del nuevo archivo de texto");
		String nombreArchivo=entrada.readLine();
			
		File f  = new File (nombreArchivo);	
		
		PrintWriter archivo =  new PrintWriter (new FileWriter ("src//ej2//"+nombreCarpeta+"//"+f+".txt"));
		System.out.println("Archivo creado");
		
		BufferedReader lector = new BufferedReader(new FileReader ("src//ej2//"+nombreCarpeta+"//"+f+".txt"));
		String leer;
	
		do {
		System.out.println();
		System.out.println("¿Qué desea hacer? \n1: Introducir en el archivo un array con 100 numeros pares \n2: Ver el conteenido del archivo  \n3: Salir");
		menu=Integer.parseInt(entrada.readLine());
		
		switch(menu) {
		
		case 1: 
				for(int i=0;i<nums.length;i++) {
					archivo.println(nums[i]);
					archivo.write("\r\n");
					
				}
					archivo.close();
			break;
		
		case 2:
			leer=lector.readLine();
			while(leer!=null) {
				
				System.out.println(leer);
				leer = lector.readLine();
			}
			archivo.close();
			
			break;
			
		case 3:
			System.out.println("Adiós");
			salida=1;
			break;
		
		}
		
		}while(salida==0);
		
	}

}
