package Ej3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Ej3 {

	public static void leer(String ruta) throws IOException {
		File f = new File(ruta);

		BufferedReader lector = new BufferedReader(new FileReader(f));
		String linea;

		linea = lector.readLine();

		while (linea != null) {

			System.out.println(linea);
			linea = lector.readLine();

		}
		lector.close();
	}

	public static void leerCaracteres(String ruta) throws IOException {
		File f = new File(ruta);

		BufferedReader lector = new BufferedReader(new FileReader(f));
		int caracter;
		
		while((caracter = lector.read()) !=-1) {
			if(caracter != ' ') {
				System.out.print((char)caracter);
			}
		}
		lector.close();
	}



}
