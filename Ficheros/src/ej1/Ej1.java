package ej1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class Ej1 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		BufferedReader entrada = new BufferedReader (new InputStreamReader (System.in));
	
		
		System.out.println("Escribe el nombre de la nueva carpeta");
	String nombreCarpeta=entrada.readLine();
		
		File carpeta = new File("src//ej1//"+nombreCarpeta);
		
		if(!carpeta.exists()) {
			carpeta.mkdir();
		}else {
			System.out.println("La carpeta indicada ya existia, se usará");
		}
		
		System.out.println("Escribe el nombre del nuevo archivo de texto");
		String nombreArchivo=entrada.readLine();
			
		File archivo  = new File (nombreArchivo);	
	
	

		PrintWriter fichero = new PrintWriter(new FileWriter ("src//ej1//"+nombreCarpeta+"//"+archivo+".txt"));
		System.out.println("Se ha creado el archivo");
	
	
	
		//File f1 = new File("src//ej1//"+nombreCarpeta+"//"+archivo+".txt");
		
	
		
		System.out.println("Escriba lo  que desee en el archivo");
		String contenido = entrada.readLine();
		
		fichero.write(contenido);
		fichero.close();
	
		BufferedReader lector  = new BufferedReader (new FileReader("src//ej1//"+nombreCarpeta+"//"+archivo+".txt"));
		String leer;
		leer=lector.readLine();
		while(leer!=null) {
			System.out.println("Lectura: "+leer);
			leer=lector.readLine();
		}
		lector.close();
		
	}

}
