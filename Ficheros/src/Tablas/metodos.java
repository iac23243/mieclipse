package Tablas;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class metodos {

	public static PrintWriter crearFichero(String nombre) throws IOException {
		File f = new File(nombre);

		PrintWriter fichero = new PrintWriter(new FileWriter("src\\Tablas\\" + f));

		return fichero;
	}

	public static void rellenarArray(int[] array) {

		Random r = new Random();

		for (int i = 0; i < array.length; i++) {
			array[i] = r.nextInt(101);
		}
	}

	public static void volcarArrayOrdenado(int[] array, PrintWriter file) {

		boolean par = false, impar = false;
		int cero = 0;

		for (int i = 0; i < array.length; i++) {

			if (array[i] % 2 == 0 && array[i]!=0) {
				if (!par) {
					file.println("Numeros pares");
					par=true;
				}
				
				if(array[i]==0) {
					cero++;
				}
				
				file.print(array[i]+", ");

			}

			file.println();

		}//fin for 
		
		
		for (int i = 0; i < array.length; i++) {

			if (array[i] % 2 != 0 && array[i]!=0) {
				if (!impar) {
					file.println("Numeros impares");
					impar=true;
				}
				
				if(array[i]==0) {
					cero++;
				}
				
				file.print(array[i]+", ");

			}
			
		} // fin for
		
		file.println();
		
		if(cero!=0) {
			file.println("Cantidad de ceros");
			file.print(cero);
		}
		
		
		file.close();
	}
}
