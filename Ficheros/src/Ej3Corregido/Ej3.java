package Ej3Corregido;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Ej3 {

	public static void leer(String ruta) throws IOException {
//		File f = new File(ruta);
		leerCaracteres(ruta);

		/*No habías llamado al metodo leerCaracteres que tenías implementado./*	



		
	 /* A mi parecer todo esto sobra, ya que has creado ya un metodo para ir leyendo
	  * 
	  *
	  * BufferedReader lector = new BufferedReader(new FileReader(f));
	  
		String linea;
		linea = lector.readLine();

		while (linea != null) {

			System.out.println(linea);
			linea = lector.readLine();

		}
		lector.close();
		*/
	}

	public static void leerCaracteres(String ruta) throws IOException {
		File f = new File(ruta);

		BufferedReader lector = new BufferedReader(new FileReader(f));
		int caracter;
		
		while((caracter = lector.read()) !=-1) {
			if(caracter != ' ') {
				//aqui elimino ln del system.out para que lo pinte todo en una linea
				System.out.print((char)caracter);
			}
		}
		lector.close();
	}

}
