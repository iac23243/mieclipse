package Ej4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Ej4 {

	public static void sumar(String ruta) throws NumberFormatException, IOException {
		File f = new File(ruta);
		
		BufferedReader lector = new BufferedReader (new FileReader (f));
		int suma = 0;
		String linea  = lector.readLine();
		
		while(linea != null) {
			System.out.println(suma+"+"+linea);
			suma=suma+Integer.parseInt(linea);
			linea=lector.readLine();
		}
		
		System.out.println("Total= "+suma);
		
	}
	
	
	
}
