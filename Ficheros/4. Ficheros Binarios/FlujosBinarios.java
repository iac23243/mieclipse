package c_EscribirDatosFichero;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
/**
 *
 * @author codesitio.com
 */
public class FlujosBinarios {
     public static void main(String[] args) throws FileNotFoundException, IOException {
      int char1;         
      FileInputStream leer;
      FileOutputStream escribir;
      
      //creamos los objetos de cada flujo de E/S
      leer = new FileInputStream("src//c_EscribirDatosFichero//datos.txt");
      escribir = new FileOutputStream("src//c_EscribirDatosFichero//copia.txt");
      
     /*
      * Leemos el primer caracter y lo almacenamos en la variable int "char1".
      * solo se utilizan los "8 bits menos significativos" de la variable int.
      * Recordemos que "el tipo int ocupa 4 bytes" (32 bits)y el tipo byte (8 bits)
      */
      char1 = leer.read();
      
     /*
      * Recorremos con un bucle while todo el fichero byte a byte hasta el final.
      * Cuando llegue al final retorna un valor -1
      */
      while(char1 != -1){          
          escribir.write(char1);
          char1 = leer.read();
      }
     System.out.println("Se ha realizado la copia del fichero datos.txt");
      
     /*
      *  Cerramos los flujos abiertos (acción siempre necesaria
      *  para liberar recursos.
      */
         leer.close();
         escribir.close();
    }       
}

