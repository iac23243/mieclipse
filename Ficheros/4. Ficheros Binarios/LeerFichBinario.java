package d_LeerDatosFichero;

import java.io.FileInputStream;
import java.io.IOException;

/*
 * La clase FileInputStream en Java amplía la clase InputStream. Usamos esta clase para leer datos en forma de bytes de un archivo. 
 * Por ejemplo, podemos usar FileInputStream para leer una imagen, archivo pdf, audio, video.
 * 
 * La clase FileOutputStream crea un fichero, salvo que exista y sea de sólo lectura. Estas clases aportan tres constructores cada una, 
 * dependiendo de la información aportada para identificar el fichero a abrir:

Un constructor que toma como argumento un String que es el nombre del fichero que se va a abrir.
Un constructor que toma un objeto de tipo File que se refiere al fichero (ver la clase File).
Un constructor que toma un objeto de tipo FileDescriptor, que constituye un valor dependiente del sistema de ficheros y que describe un fichero abierto.

 * programa lee el fichero de datos escrito en el programa anterior y lo muestra por pantalla. Para eller bytes de un fichero utilizamos la clase FileInputStream que hereda de la clase InputStream.
import java.io.*;  */

public class LeerFichBinario{
	public static void main(String[] args){
		FileInputStream salida=null;
		String s="";
		char c;
		
		try{
			salida=new FileInputStream("src//d_LeerDatosFichero//datos.txt");
			int size = salida.available();
		
			for (int i=0;i<size;i++){
				c=(char)salida.read();
				s=s+c;
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		finally{
			System.out.println(s);
			try{
				salida.close();
			}catch(IOException e){ 
				e.printStackTrace();
			}
		}
		
		
	}
}