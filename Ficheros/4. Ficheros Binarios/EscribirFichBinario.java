package c_EscribirDatosFichero;

import java.io.*;

/*Como se puede observar, en el código  se crea un objeto de la clase FileOutputStream llamado f y se inicializa a null. 
 * En el siguiente código se crea una instancia del objeto llamando al constructor: f=new FileOutputStream(“datos.txt”);*/
public class EscribirFichBinario{
	public static void main(String[] args){ 
		FileOutputStream f=null;
		String s = "En un lugar de la mancha de cuyo nombre no quiero acordarme..."; 
		char c=0;
		try{
			f=new FileOutputStream("src/c_EscribirDatosFichero/datos.txt");
			for (int i=0; i<s.length();i++){
				c=s.charAt(i);
				f.write((byte)c);
			}
		}catch(IOException e){
			e.printStackTrace();
		}

		finally{
			try{
				f.close();
				}catch(IOException e){ 
					e.printStackTrace();
			}
		}
		System.out.println("Se ha escrito en el fichero");

	}
}