package f_FileReaderYFileWriter;

import java.io.*;
/**
 * 
 * FileWriter y FileReader trabajan con flujos de caracteres (char) en modo lectura y escritura sobre un fichero
 * El programa tiene almacenado una serie de personas en una estructura amigos que es un vector de objetos String. 
 * Los datos en este vector se graban en un fichero “amigos.txt”, cada persona en una línea y para ello se graba en el fichero el retorno de carro 
 * y línea “\r\n” (\r es el retorno de carro y \n es el carácter de nueva línea).
 * Para la lectura del fichero se utiliza un objeto FileReader. Como queremos leer línea a línea utilizamos un objeto de la clase BufferedReader 
 * que tiene el método readLine(), que permite leer una línea de un flujo de entrada.
 * 
*/
public class FicheroTexto{
	public static void main(String[] args){ 
		
		String []amigos={"Andrés Rosique","Pedro Ruiz","Isaac Sanchez","Juan Serrano"}; 
/*		//si no ponemos ruta lo guardaría en la raiz del proyecto de java
		File fs = new File("amigos.txt");
*/	
		File fs = new File("src//f_FileReaderYFileWriter//amigos.txt");

		
		try{
			FileWriter fw = new FileWriter(fs);
			for (String s : amigos){
				/*
				 * el metodo write escribe una porcion de un String. Los argumentos son: 
				 *Buffer de caracteres
				 *Indica la distancia (desplazamiento) desde el inicio del objeto hasta un punto o elemento dado, presumiblemente dentro del mismo objeto.
				 *numero de caracteres a escribir
				 */				

				fw.write(s,0,s.length());
				fw.write("\r\n");
			}
			//no debenos olvidarnos de cerrar siempre el flujo cuando acabemos de manejarlo
			if (fw != null) 
				fw.close();
			
		}catch(IOException e){
			e.printStackTrace();
		}
/*		//si no ponemos ruta accederia a la raiz del proyecto de java para buscar el fichero	
		File fe = new File("amigos.txt");
*/		
		File fe = new File("src//f_FileReaderYFileWriter//amigos.txt");

		if (fe.exists()){
			try{
				FileReader fr = new FileReader(fe);
				BufferedReader br = new BufferedReader(fr);
				String s;
				while((s = br.readLine()) != null) {
					System.out.println(s);
				}
				if (fr != null) 
					fr.close();
				
			}catch(IOException e){
			e.printStackTrace();
			}
		}
	}
}