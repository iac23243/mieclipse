package ej1;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Window.Type;

public class ej1 extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private String nombre;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ej1 frame = new ej1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ej1() {
		setTitle("Saludador");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(128, 255, 128));
		contentPane.setForeground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextPane entrada = new JTextPane();
		entrada.setBounds(125, 93, 187, 20);
		contentPane.add(entrada);
		
		
		JButton boton = new JButton("Saludar");
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nombre=entrada.getText();
				JOptionPane.showMessageDialog(null, "Hola "+nombre);
			}
		});
		boton.setBounds(166, 141, 93, 23);

		contentPane.add(boton);
		
		JLabel etiquetaEscribe = new JLabel("Escribe un nombre para saludar");
		etiquetaEscribe.setFont(new Font("Tahoma", Font.PLAIN, 15));
		etiquetaEscribe.setBounds(115, 50, 232, 20);
		contentPane.add(etiquetaEscribe);
	}

}
