package ej3;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class ej3 extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private String so="";
	private String especialidad="";
	private boolean admin=false;
	private boolean prog=false;
	private boolean dg=false;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ej3 frame = new ej3();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ej3() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 361, 438);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel EtiquetaSO = new JLabel("Elige un SO");
		EtiquetaSO.setBounds(29, 21, 90, 22);
		EtiquetaSO.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPane.add(EtiquetaSO);
		
		JLabel etiquetaEsp = new JLabel("Elige una especialidad");
		etiquetaEsp.setBounds(181, 21, 131, 22);
		etiquetaEsp.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPane.add(etiquetaEsp);
		
		/*
		 * CheckBox
		 */
		JCheckBox chckbxPrg = new JCheckBox("Programación");
		chckbxPrg.addChangeListener(new ChangeListener() {
			
			public void stateChanged(ChangeEvent e) {
				if(!prog) {
					prog=true;
					especialidad=especialidad+" programacion";
				}else {
					prog=false;
				especialidad=especialidad.replaceAll(" programacion", "");
				}
				
			}
	
		});
		chckbxPrg.setBounds(196, 50, 116, 22);
		contentPane.add(chckbxPrg);
		
		JCheckBox chckbxDG = new JCheckBox("Diseño gráfico");
		chckbxDG.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(!dg) {
					dg=true;
					especialidad=especialidad+" diseño grafico";
				}else {
					dg=false;
				especialidad=especialidad.replaceAll(" diseño grafico", "");
				}
			}
		});
		chckbxDG.setBounds(196, 75, 116, 22);
		contentPane.add(chckbxDG);
		
		JCheckBox chckbxAdmin = new JCheckBox("Administración");
		chckbxAdmin.setBounds(196, 103, 116, 22);
		contentPane.add(chckbxAdmin);
		chckbxAdmin.addChangeListener(new ChangeListener() {
			
			public void stateChanged(ChangeEvent e) {
				if(!admin) {
					admin=true;
					especialidad=especialidad+" administracion";
				}else {
					admin=false;
				especialidad=especialidad.replaceAll(" administracion", "");
				}
				
			}
	
		});
		
		/*
		 * RadioButton
		 */
		
		JRadioButton rdbtnWs = new JRadioButton("Window");
		
		rdbtnWs.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				so=rdbtnWs.getText();
			}
		});
		
		rdbtnWs.setBounds(29, 50, 109, 23);
		contentPane.add(rdbtnWs);
		
		JRadioButton rdbtnLinux = new JRadioButton("Linux");
		rdbtnLinux.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				so=rdbtnLinux.getText();
			}
		});
		rdbtnLinux.setBounds(29, 75, 109, 23);
		contentPane.add(rdbtnLinux);
		
		JRadioButton rdbtnMac = new JRadioButton("Mac");
		rdbtnMac.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				so=rdbtnMac.getText();
			}
		});
		rdbtnMac.setBounds(29, 103, 109, 23);
		contentPane.add(rdbtnMac);
		
		/*
		 * Grupo de radioButton
		 * 
		 */
		
		ButtonGroup btngp = new ButtonGroup();
		btngp.add(rdbtnWs);
		btngp.add(rdbtnMac);
		btngp.add(rdbtnLinux);
		
		
	
		
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 151, 313, 2);
		contentPane.add(separator);

		
		JLabel numeroSlider = new JLabel("");
		numeroSlider.setBounds(48, 211, 31, 14);
		contentPane.add(numeroSlider);
		
		JSlider slider = new JSlider();
		slider.setBounds(66, 211, 200, 26);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setMaximum(10);
		contentPane.add(slider);
		slider.addChangeListener(new ChangeListener() {

		
			public void stateChanged(ChangeEvent e) {
				numeroSlider.setText(""+slider.getValue());
				
			}
			
			
		});
		
		JLabel etiquetaSlider = new JLabel("Indique las horas que pasa en el ordenador");
		etiquetaSlider.setBounds(38, 178, 255, 22);
		etiquetaSlider.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPane.add(etiquetaSlider);
		
		JButton generar = new JButton("Generar");
		generar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JOptionPane.showMessageDialog(null, "Tu sistema operativo preferido es "+so+", tu especialidad/es son:  "+especialidad+". Pasas "+slider.getValue()+" horas en el pc" );
			}
		});
		generar.setBounds(118, 279, 89, 23);
		contentPane.add(generar);
		
		
	}
}
