package Ej5;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import javax.swing.JSpinner;
import javax.swing.JList;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JComboBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ej5 extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ej5 frame = new ej5();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ej5() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 398, 287);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(35, 125, 313, 2);
		contentPane.add(separator);
		
		JLabel LabelOriginal = new JLabel("Original");
		LabelOriginal.setBounds(22, 11, 46, 14);
		contentPane.add(LabelOriginal);
		
		JLabel LabelEspejo = new JLabel("Espejo");
		LabelEspejo.setBounds(35, 138, 46, 14);
		contentPane.add(LabelEspejo);
		
		/**
		 * Radiobutton espejo
		 */
		
		JRadioButton rdbtnEspejo1 = new JRadioButton("Opcion 1");
		rdbtnEspejo1.setEnabled(false);
		rdbtnEspejo1.setBounds(32, 159, 109, 23);
		contentPane.add(rdbtnEspejo1);
		
		JRadioButton rdbtnEspejo2 = new JRadioButton("Opcion 2");
		rdbtnEspejo2.setEnabled(false);
		rdbtnEspejo2.setBounds(32, 185, 109, 23);
		contentPane.add(rdbtnEspejo2);
		
		JRadioButton rdbtnEspejo3 = new JRadioButton("Opcion 3");
		rdbtnEspejo3.setEnabled(false);
		rdbtnEspejo3.setBounds(32, 211, 109, 23);
		contentPane.add(rdbtnEspejo3);
		/**
		 * Radiobutton grupoEspejo
		 */
		ButtonGroup grupoEspejo = new ButtonGroup();
		grupoEspejo.add(rdbtnEspejo1);
		grupoEspejo.add(rdbtnEspejo2);
		grupoEspejo.add(rdbtnEspejo3);
		
		
		
		/**
		 * Radiobutton original
		 */
	
		JRadioButton rdbtn1 = new JRadioButton("Opcion 1");
		
		rdbtn1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				
				if(rdbtn1.isSelected()) {
					rdbtnEspejo1.setSelected(true);
				}else {
					rdbtnEspejo1.setSelected(false);
				}
				
			}		
		});
		
		rdbtn1.setBounds(32, 32, 109, 23);
		contentPane.add(rdbtn1);
		
		/**
		 * 
		 */
		
		JRadioButton rdbtn2 = new JRadioButton("Opcion 2");
		
		rdbtn2.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				
				if(rdbtn2.isSelected()) {
					rdbtnEspejo2.setSelected(true);
				}else {
					rdbtnEspejo2.setSelected(false);
				}
				
			}		
		});
		rdbtn2.setBounds(32, 58, 109, 23);
		contentPane.add(rdbtn2);
		
		/**
		 * 
		 */
		
		JRadioButton rdbtn3 = new JRadioButton("Opcion 3");
		rdbtn3.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				
				if(rdbtn3.isSelected()) {
					rdbtnEspejo3.setSelected(true);
				}else {
					rdbtnEspejo3.setSelected(false);
				}
				
			}		
		});
		rdbtn3.setBounds(32, 84, 109, 23);
		contentPane.add(rdbtn3);
	
		/**
		 * Radiobutton grupo
		 */
		ButtonGroup grupo = new ButtonGroup();
		grupo.add(rdbtn3);
		grupo.add(rdbtn2);
		grupo.add(rdbtn1);
		
	
	
		/**
		 * CheckBox espejo
		 */

		JCheckBox chckbxEspejo1 = new JCheckBox("Opcion 4");
		chckbxEspejo1.setEnabled(false);
		chckbxEspejo1.setBounds(162, 159, 97, 23);
		contentPane.add(chckbxEspejo1);
		
		JCheckBox chckbxEspejo2 = new JCheckBox("Opcion 5");
		chckbxEspejo2.setEnabled(false);
		chckbxEspejo2.setBounds(162, 185, 97, 23);
		contentPane.add(chckbxEspejo2);
		
		JCheckBox chckbxEspejo3 = new JCheckBox("Opcion 6");
		chckbxEspejo3.setEnabled(false);
		chckbxEspejo3.setBounds(162, 211, 97, 23);
		contentPane.add(chckbxEspejo3);
		
		/**
		 * 
		 * CheckBox original
		 */
	
		JCheckBox chckbx1 = new JCheckBox("Opcion 4");
		chckbx1.addChangeListener(new ChangeListener() {
		
			public void stateChanged(ChangeEvent e) {
				if(chckbx1.isSelected()) {
					chckbxEspejo1.setSelected(true);
				}else {
					chckbxEspejo1.setSelected(false);
				}
			}
		});
		
		chckbx1.setBounds(162, 32, 97, 23);
		contentPane.add(chckbx1);
		
		/**
		 * chckbx2
		 */
	
		
		JCheckBox chckbx2 = new JCheckBox("Opcion 5");
		chckbx2.addChangeListener(new ChangeListener() {
			
			public void stateChanged(ChangeEvent e) {
				if(chckbx2.isSelected()) {
					chckbxEspejo2.setSelected(true);
				}else {
					chckbxEspejo2.setSelected(false);
				}
			}
		});
		chckbx2.setBounds(162, 58, 97, 23);
		contentPane.add(chckbx2);
	
		/**
		 * chckbx3
		 */
	
		JCheckBox chckbx3 = new JCheckBox("Opcion 6");
		chckbx3.addChangeListener(new ChangeListener() {
			
			public void stateChanged(ChangeEvent e) {
				if(chckbx3.isSelected()) {
					chckbxEspejo3.setSelected(true);
				}else {
					chckbxEspejo3.setSelected(false);
				}
			}
		});
		chckbx3.setBounds(162, 84, 97, 23);
		contentPane.add(chckbx3);
		
		/**
		 * Los  textArea
		 * 
		 */
		
		JTextArea textAreaEspejo = new JTextArea();
		textAreaEspejo.setEnabled(false);
		textAreaEspejo.setEditable(false);
		textAreaEspejo.setBounds(265, 158, 90, 22);
		contentPane.add(textAreaEspejo);
		
		JTextArea textArea = new JTextArea();
		textArea.addKeyListener(new KeyAdapter() {
			
			public void keyPressed(KeyEvent e) {
				textAreaEspejo.setText(textArea.getText());
			}
		});
		
		textArea.setBounds(265, 31, 90, 22);
		contentPane.add(textArea);
		
		/**
		 * Spinners
		 */
	
		JSpinner spinnerEspejo = new JSpinner();
		spinnerEspejo.setEnabled(false);
		spinnerEspejo.setBounds(265, 212, 90, 20);
		contentPane.add(spinnerEspejo);
		
		JSpinner spinner = new JSpinner();
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				
				spinnerEspejo.setValue(spinner.getValue());
			}
		});
		spinner.setBounds(265, 85, 90, 20);
		contentPane.add(spinner);
		
		JComboBox comboBoxEspejo = new JComboBox();
		comboBoxEspejo.setEnabled(false);
		comboBoxEspejo.setBounds(265, 185, 90, 22);
		contentPane.add(comboBoxEspejo);
		comboBoxEspejo.addItem("LMY");	
		comboBoxEspejo.addItem("PRG");
		comboBoxEspejo.addItem("BD");
		comboBoxEspejo.addItem("ED");
		
		
		JComboBox comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				comboBoxEspejo.setSelectedItem(comboBox.getSelectedItem());
			}
		});
		comboBox.setBounds(265, 58, 90, 22);
		contentPane.add(comboBox);
		comboBox.addItem("LMY");	
		comboBox.addItem("PRG");
		comboBox.addItem("BD");
		comboBox.addItem("ED");
		
		
		
		
		
	}
}
