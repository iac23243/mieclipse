package ej2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import java.awt.Color;

public class ej2 extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private String[] peliculas = new String[8];
	private String entrada;
	private int cont=0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ej2 frame = new ej2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ej2() {
		setForeground(new Color(128, 128, 255));
		setBackground(new Color(255, 255, 255));
		setTitle("Mis Pelis");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel etiquetaTextoPeliculas = new JLabel("Nombre de la película (máximo 8)");
		etiquetaTextoPeliculas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		etiquetaTextoPeliculas.setBounds(25, 42, 133, 20);
		contentPane.add(etiquetaTextoPeliculas);

		JTextPane texto = new JTextPane();
		texto.setBounds(25, 73, 144, 20);
		contentPane.add(texto);

		JLabel etiquetaPeliculas = new JLabel("Peliculas");
		etiquetaPeliculas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		etiquetaPeliculas.setBounds(304, 46, 64, 14);
		contentPane.add(etiquetaPeliculas);

		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(259, 73, 133, 22);
		contentPane.add(comboBox);

		JButton anyadir = new JButton("Añadir");
		anyadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(cont<8) {
					entrada = texto.getText();
					peliculas[cont]=entrada;
					comboBox.addItem(peliculas[cont]);
					cont++;
					texto.setText("");
				}else {
					JOptionPane.showMessageDialog(null,"Alcanzó número máximo de películas almacenadas","Error",0);
				}

			}
		});
		anyadir.setBounds(43, 117, 89, 23);
		contentPane.add(anyadir);
	}
}
