package ej4;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;

public class ej4 extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private Random num = new Random();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ej4 frame = new ej4();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ej4() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 325, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel etiquetaNum2 = new JLabel("Valor máximo");
		etiquetaNum2.setBounds(22, 80, 89, 17);
		contentPane.add(etiquetaNum2);
		
		JLabel etiquetaNum1 = new JLabel("Valor base");
		etiquetaNum1.setBounds(22, 30, 89, 17);
		contentPane.add(etiquetaNum1);
		
		JLabel etiquetaNumGen = new JLabel("Número generado");
		etiquetaNumGen.setBounds(22, 140, 89, 17);
		contentPane.add(etiquetaNumGen);
		
		JSpinner num1 = new JSpinner();
		num1.setBounds(133, 30, 48, 20);
		contentPane.add(num1);
		
		JSpinner num2 = new JSpinner();
		num2.setBounds(133, 80, 48, 20);
		contentPane.add(num2);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(121, 138, 59, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnGenerar = new JButton("Generar");
		btnGenerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
		int	numero1=(int)num1.getValue();
		int	numero2=(int)num2.getValue();
		Integer generado = num.nextInt(numero1,numero2);
				
			
				textField.setText(generado.toString());
			}
		});
		btnGenerar.setBounds(103, 181, 89, 23);
		contentPane.add(btnGenerar);
	}
}
