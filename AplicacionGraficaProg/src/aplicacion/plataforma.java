package aplicacion;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

public class plataforma extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	boolean esJuego=false;
	boolean esPeli=false;
	boolean esSerie=false;
	protected String nombre;
	
	
	
	public boolean isEsJuego() {
		return esJuego;
	}



	public boolean isEsPeli() {
		return esPeli;
	}



	public boolean isEsSerie() {
		return esSerie;
	}



	public String getNombre() {
		return nombre;
	}



	/**
	 * Create the dialog.
	 */
	public plataforma( boolean modal) {
		super ();
		
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblNombre = new JLabel("Nombre de la plataforma");
			lblNombre.setBounds(10, 11, 125, 14);
			contentPanel.add(lblNombre);
		}
		

		
		JLabel lblTipo = new JLabel("Tipo de contenido");
		lblTipo.setBounds(10, 96, 148, 14);
		contentPanel.add(lblTipo);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(10, 41, 135, 20);
		contentPanel.add(textPane);
		
		JCheckBox chckbxJuegos = new JCheckBox("Video Juegos");
		chckbxJuegos.setBounds(6, 117, 97, 23);
		contentPanel.add(chckbxJuegos);
		
		JCheckBox chckbxPeliculas = new JCheckBox("Peliculas");
		chckbxPeliculas.setBounds(6, 143, 97, 23);
		contentPanel.add(chckbxPeliculas);
		
		chckbxJuegos.isSelected();
		
		JCheckBox chckbxSeries = new JCheckBox("Series");
		chckbxSeries.setBounds(6, 171, 97, 23);
		contentPanel.add(chckbxSeries);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						nombre= textPane.getText();
						
						if(chckbxJuegos.isSelected()) {
							esJuego=true;
						}
						
						if(chckbxPeliculas.isSelected()) {
							esPeli=true;
						}
						
						if(chckbxSeries.isSelected()) {
							esSerie=true;
						}
						
						
						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
