package aplicacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTabbedPane;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import javax.swing.JSpinner;
import javax.swing.JSlider;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.sql.SQLException;

public class app extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblOpinionAnyadir;
	private JTextField textFieldTituloAnyadir;
	private JTextField textFieldDirectorAnyadir;
	private JTextField textFieldDuracionAnyadir;
	private JTextField textFieldAnyoAnyadir;
	private JTextField textField;
	private conexion bd = new conexion();


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					app frame = new app();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public app() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 822, 486);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPanel = new JTabbedPane(JTabbedPane.TOP);
		tabbedPanel.setBounds(10, 11, 796, 425);
		contentPane.add(tabbedPanel);

		JPanel panelBuscar = new JPanel();
		tabbedPanel.addTab("Buscar", null, panelBuscar, null);
		panelBuscar.setLayout(null);

		/**
		 * 
		 * 
		 * Panel de anyadir contenido
		 * 
		 * 
		 * 
		 */
		JPanel anyadir = new JPanel();
		tabbedPanel.addTab("Añadir Contenido", null, anyadir, null);
		anyadir.setLayout(null);

		/**
		 * Esta comboBox definirá si el tipo de contenido anyadido es un juego,pelicula o serie
		 */
		JComboBox comboBoxTipo = new JComboBox();
		comboBoxTipo.setBounds(127, 11, 192, 22);
		anyadir.add(comboBoxTipo);
		comboBoxTipo.addItem("Juego");
		comboBoxTipo.addItem("Pelicula");
		comboBoxTipo.addItem("Serie");
		
		

		
		/*
		 * Conjunto de lbl
		 */

		JLabel lblTituloAnyadir = new JLabel("Titulo");
		lblTituloAnyadir.setBounds(10, 57, 46, 14);
		anyadir.add(lblTituloAnyadir);

		JLabel lblEstrenoAnyadir = new JLabel("Año de estreno (yyyy)");
		lblEstrenoAnyadir.setBounds(10, 101, 154, 14);
		anyadir.add(lblEstrenoAnyadir);

		JLabel lblNotaAnyadir = new JLabel("Nota");
		lblNotaAnyadir.setBounds(10, 143, 46, 14);
		anyadir.add(lblNotaAnyadir);

		JLabel lblGeneroAnyadir = new JLabel("Genero");
		lblGeneroAnyadir.setBounds(10, 193, 46, 14);
		anyadir.add(lblGeneroAnyadir);

		JLabel lblPlataformaAnyadir = new JLabel("Plataforma");
		lblPlataformaAnyadir.setBounds(10, 285, 77, 14);
		anyadir.add(lblPlataformaAnyadir);

		JLabel lblDirectorAnyadir = new JLabel("Director");
		lblDirectorAnyadir.setBounds(10, 238, 46, 14);
		anyadir.add(lblDirectorAnyadir);

		JLabel lblSinopsisAnyadir = new JLabel("Sinopsis");
		lblSinopsisAnyadir.setBounds(382, 15, 59, 14);
		anyadir.add(lblSinopsisAnyadir);
/*
 * texPanel para la sinopsis
 */
		
		JTextPane textPaneSinopsisAnyadirAnyadir = new JTextPane();
		textPaneSinopsisAnyadirAnyadir.setBounds(382, 40, 352, 83);
		anyadir.add(textPaneSinopsisAnyadirAnyadir);

		lblOpinionAnyadir = new JLabel("Opinion");
		lblOpinionAnyadir.setBounds(382, 143, 46, 14);
		anyadir.add(lblOpinionAnyadir);

		textFieldTituloAnyadir = new JTextField();
		textFieldTituloAnyadir.setBounds(83, 50, 236, 28);
		anyadir.add(textFieldTituloAnyadir);
		textFieldTituloAnyadir.setColumns(10);

		JTextPane textPaneOpinionAnyadir = new JTextPane();
		textPaneOpinionAnyadir.setBounds(382, 168, 352, 124);
		anyadir.add(textPaneOpinionAnyadir);

	

		JComboBox comboBoxGeneroAnyadir = new JComboBox();
		comboBoxGeneroAnyadir.setBounds(108, 189, 211, 22);
		anyadir.add(comboBoxGeneroAnyadir);

		textFieldDirectorAnyadir = new JTextField();
		textFieldDirectorAnyadir.setBounds(108, 235, 209, 20);
		anyadir.add(textFieldDirectorAnyadir);
		textFieldDirectorAnyadir.setColumns(10);

		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(108, 281, 208, 22);
		anyadir.add(comboBox);

		JLabel lblDuracionAnyadir = new JLabel("Duracion");
		lblDuracionAnyadir.setBounds(10, 338, 59, 14);
		anyadir.add(lblDuracionAnyadir);

		textFieldDuracionAnyadir = new JTextField();
		textFieldDuracionAnyadir.setBounds(83, 335, 111, 20);
		anyadir.add(textFieldDuracionAnyadir);
		textFieldDuracionAnyadir.setColumns(10);

		JLabel lblMinAnyadir = new JLabel("minutos");
		lblMinAnyadir.setBounds(204, 338, 46, 14);
		anyadir.add(lblMinAnyadir);

		JButton btnAnyadir = new JButton("Añadir");
		btnAnyadir.setBounds(510, 334, 89, 23);
		anyadir.add(btnAnyadir);

		textFieldAnyoAnyadir = new JTextField();
		textFieldAnyoAnyadir.setBounds(174, 98, 142, 20);
		anyadir.add(textFieldAnyoAnyadir);
		textFieldAnyoAnyadir.setColumns(10);
		
		JLabel lblTipoObraAnyadir = new JLabel("Tipo de obra");
		lblTipoObraAnyadir.setBounds(10, 15, 98, 14);
		anyadir.add(lblTipoObraAnyadir);
		
		/*
		 * Slider de la nota y su lbl para el numero
		 */
		
		JLabel lblSliderAnyadir = new JLabel("");
		lblSliderAnyadir.setBounds(52, 143, 46, 14);
		anyadir.add(lblSliderAnyadir);
		
		JSlider sliderNotaAnyadir = new JSlider();
		sliderNotaAnyadir.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				
				lblSliderAnyadir.setText(sliderNotaAnyadir.getValue()+"");
			}
		});
		sliderNotaAnyadir.setMaximum(10);
		sliderNotaAnyadir.setBounds(108, 131, 211, 26);
		anyadir.add(sliderNotaAnyadir);
		
		
		/**
		 * 
		 * Panel otros
		 * 
		 */
		JPanel panelOtros = new JPanel();
		tabbedPanel.addTab("Añadir Otros", null, panelOtros, null);
		panelOtros.setLayout(null);

		JLabel lblSeleccionOtros = new JLabel("Seleccione que desea añadir");
		lblSeleccionOtros.setBounds(6, 20, 172, 23);
		panelOtros.add(lblSeleccionOtros);

		/*
		 * RadioButtons
		 */

		ButtonGroup buttonGroup = new ButtonGroup();

		/**
		 * Introducir genero
		 */
		JRadioButton rdbtnOtrosGenero = new JRadioButton("Genero");
		rdbtnOtrosGenero.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String nombre = JOptionPane.showInputDialog(null, "Introduce el nombre del género");

				if(nombre!=null) {
					try {
						bd.InsertarGenero(nombre);
					} catch (NumberFormatException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		rdbtnOtrosGenero.setBounds(6, 50, 109, 23);
		panelOtros.add(rdbtnOtrosGenero);

		/**
		 * Introducir Director
		 * 
		 */
		JRadioButton rdbtnOtrosDirector = new JRadioButton("Director");
		rdbtnOtrosDirector.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				boolean control = true;
				int edad = 0;
				String nombre = JOptionPane.showInputDialog(null, "Introduce el nombre del director");

				do {
					try {
						edad = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce la edad del director"));
						control = true;

					} catch (NumberFormatException e1) {
						control = false;
					}

				} while (!control | edad==0);

				try {
					bd.InsertarDirector(nombre, edad);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		rdbtnOtrosDirector.setBounds(6, 76, 109, 23);
		panelOtros.add(rdbtnOtrosDirector);

		/**
		 * Introducir plataforma
		 */

	JRadioButton rdbtnOtrosPlataforma = new JRadioButton("Plataforma");
		rdbtnOtrosPlataforma.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			plataforma platform = new plataforma(true);
			platform.setVisible(true);
				
			}
		});
		rdbtnOtrosPlataforma.setBounds(6, 102, 109, 23);
		panelOtros.add(rdbtnOtrosPlataforma);
		
		buttonGroup.add(rdbtnOtrosGenero);
		buttonGroup.add(rdbtnOtrosDirector);
		buttonGroup.add(rdbtnOtrosPlataforma);

	}
}
