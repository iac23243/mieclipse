package aplicacion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class conexion {

	//creamos 3 variables que llaman a unas interfaces
	private Connection con; //Para crear la conexion entre la aplicacion y la BD
	private Statement st;	//Para establecer la conexion. Representa una sentencia SQL que ejecutará el servidor de la BD 
	PreparedStatement stmt = null; //Tambien representa una sentencia SQL, que permite configurar o parametrizar facilmente valores en la consulta.
	private ResultSet rs;   //Representa una tabla con el resultado que genera el SGBD tras ejecutar una sentencia de consulta
	
	public conexion() {
		
		try {
			//aquí nos vamos a conectar a la BD. Para eso escribiremos este bloque: 
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			/*  Utilizamos la primera variable que decalramos anteriomente que nos servirá para almacenar la conexion con la BD.
			 *  A continuación de la libreria que estamos utilizando (jdbc:mysql) ponemos el host y el puerto que estamos utilizando. Esa información la tenemos disponible en nuestra aplicacion de servidor.
			 *  A continuación el nombre de la base de datos a la que nos vamos a conectar,
			 *  y por ultimo ponemos el valor del usuario y de la contraseña de dicha BD
			 */
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/proyectoprg", "root","");
			
			//para establecer la conexion
			st= con.createStatement();
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	public void InsertarGenero(String nombre) throws SQLException, NumberFormatException, IOException {
		 try{
				
		        
		        /**
		         * Una vez que tenemos la conexión preparamos la sentencia. Esto lo hacemos apoyándonos en la clase PreparedStatement. 
		         * El PreparedStatement se utiliza cuando se va a realizar una sustitución de alguno de los valores de la condición, si no,
		         *  se podría utilizar un Statement o directamente ejecutar la sentencia
		         * */
		        
		        //Si queremos que el usuario nos de los datos sería así: 
			
		                     
		        stmt = con.prepareStatement("INSERT INTO generos (nombre) VALUES (?)");
		        stmt.setString(1,nombre);
		
				/*
				 * Ya solo nos quedará ejecutar la sentencia. Cuando la sentencia a ejecutar no devuelve un conjunto de resultados 
				 * no debemos de usar executeQuery(), sino que deberemos de utilizar executeUpdate(). 
				 * Esto es aplicable a INSERT, UPDATE y DELETE.
				 * 
				 * */
		        
		        
		        int retorno = stmt.executeUpdate();
		        if (retorno>0)
		           System.out.println("Insertado correctamente");   
		        else {
			           System.out.println("Error al Insertar");   

		        }
		                     
	     } catch (SQLException sqle){
	        System.out.println("SQLState: " 
	           + sqle.getSQLState());
	        System.out.println("SQLErrorCode: " 
	           + sqle.getErrorCode());
	        sqle.printStackTrace();
	     } catch (Exception e){
	        e.printStackTrace();
	     } finally {
	        if (con != null) {
	           try{
	              stmt.close();
	 //             con.close();  //Si solo fueramos a insertar, tendríamos que cerrar la BD, pero en este caso no lo hacemos puesto que luego vamos a mostrar toda la tabla
	           } catch(Exception e){
	              e.printStackTrace();
	           }
	        }
	     }		
		}//fin del metodo InsertarGenero()
	
	public void InsertarDirector(String nombre, int edad) throws SQLException, NumberFormatException, IOException {
		 try{
				
		        
		        /**
		         * Una vez que tenemos la conexión preparamos la sentencia. Esto lo hacemos apoyándonos en la clase PreparedStatement. 
		         * El PreparedStatement se utiliza cuando se va a realizar una sustitución de alguno de los valores de la condición, si no,
		         *  se podría utilizar un Statement o directamente ejecutar la sentencia
		         * */
		        
		        //Si queremos que el usuario nos de los datos sería así: 
			
		                     
		        stmt = con.prepareStatement("INSERT INTO directores (nombre,edad) VALUES (?,?)");
		        stmt.setString(1,nombre);
		        stmt.setInt(2,edad);
		
				/*
				 * Ya solo nos quedará ejecutar la sentencia. Cuando la sentencia a ejecutar no devuelve un conjunto de resultados 
				 * no debemos de usar executeQuery(), sino que deberemos de utilizar executeUpdate(). 
				 * Esto es aplicable a INSERT, UPDATE y DELETE.
				 * 
				 * */
		        
		        
		        int retorno = stmt.executeUpdate();
		        if (retorno>0)
		           System.out.println("Insertado correctamente");   
		        else {
			           System.out.println("Error al Insertar");   

		        }
		                     
	     } catch (SQLException sqle){
	        System.out.println("SQLState: " 
	           + sqle.getSQLState());
	        System.out.println("SQLErrorCode: " 
	           + sqle.getErrorCode());
	        sqle.printStackTrace();
	     } catch (Exception e){
	        e.printStackTrace();
	     } finally {
	        if (con != null) {
	           try{
	              stmt.close();
	 //             con.close();  //Si solo fueramos a insertar, tendríamos que cerrar la BD, pero en este caso no lo hacemos puesto que luego vamos a mostrar toda la tabla
	           } catch(Exception e){
	              e.printStackTrace();
	           }
	        }
	     }		
		}//fin del metodo InsertarDirector()
	
	public void InsertarPlataforma(String nombre) throws SQLException, NumberFormatException, IOException {
		 try{
				
		        
		        /**
		         * Una vez que tenemos la conexión preparamos la sentencia. Esto lo hacemos apoyándonos en la clase PreparedStatement. 
		         * El PreparedStatement se utiliza cuando se va a realizar una sustitución de alguno de los valores de la condición, si no,
		         *  se podría utilizar un Statement o directamente ejecutar la sentencia
		         * */
		        
		        //Si queremos que el usuario nos de los datos sería así: 
			
		                     
		        stmt = con.prepareStatement("INSERT INTO plataformas (nombre) VALUES (?)");
		        stmt.setString(1,nombre);
		
				/*
				 * Ya solo nos quedará ejecutar la sentencia. Cuando la sentencia a ejecutar no devuelve un conjunto de resultados 
				 * no debemos de usar executeQuery(), sino que deberemos de utilizar executeUpdate(). 
				 * Esto es aplicable a INSERT, UPDATE y DELETE.
				 * 
				 * */
		        
		        
		        int retorno = stmt.executeUpdate();
		        if (retorno>0)
		           System.out.println("Insertado correctamente");   
		        else {
			           System.out.println("Error al Insertar");   

		        }
		                     
	     } catch (SQLException sqle){
	        System.out.println("SQLState: " 
	           + sqle.getSQLState());
	        System.out.println("SQLErrorCode: " 
	           + sqle.getErrorCode());
	        sqle.printStackTrace();
	     } catch (Exception e){
	        e.printStackTrace();
	     } finally {
	        if (con != null) {
	           try{
	              stmt.close();
	 //             con.close();  //Si solo fueramos a insertar, tendríamos que cerrar la BD, pero en este caso no lo hacemos puesto que luego vamos a mostrar toda la tabla
	           } catch(Exception e){
	              e.printStackTrace();
	           }
	        }
	     }		
		}//fin del metodo InsertarGenero()
	
	
}
