package iterator;

/*Vamos a desarrollar el siguiente ejercicio: a partir de la clase Persona, vamos a
crear una lista de Personas donde tengamos un conjunto de Personas con sus
alturas correspondientes. A modo de estadística queremos calcular la estatura
media de ese conjunto de Personas.*/

public class Persona{
	public int idPersona;
	public String nombre;
	public int altura;
	
	public Persona(int idPersona, String nombre, int altura) {
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.altura = altura;
	}
	
	@Override
	public String toString() {
		return "Persona-> ID: "+idPersona+
				" \nNombre: "+nombre+
				" \nAltura: "+altura+"\n";
	}
}