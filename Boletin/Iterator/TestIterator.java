package iterator;

import java.util.Iterator;

public class TestIterator {
	public static void main (String arg[]) {
		ListaPersonas lp = new ListaPersonas();
		
		//Creamos un iterator de la clase Persona 
		Iterator<Persona> it;
		//importamos la clase persona del ejercicio anterior
		Persona e; // Este objeto lo usaremos para almacenar temporalmente objetos Persona
		
		lp.listapersona.add(new Persona(1,"Maria",175));
		lp.listapersona.add(new Persona(2,"Carla",160));
		lp.listapersona.add(new Persona(3,"Enriqueta",190));
		
		System.out.println("Lista antes de recorrer/eliminar: "+lp.toString());
		it = lp.iterator();
		
		while (it.hasNext() ) 
		{
			e = it.next();
			if (e.altura<170) 
			{
				it.remove(); 
			} // Si se invoca it.remove() se elimina a la persona de la colección
		}
		System.out.println("Lista después de recorrer/eliminar: " + lp.toString() );
	}
}