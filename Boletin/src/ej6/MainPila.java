package ej6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainPila {

	public static void main(String[] args) throws NumberFormatException, IOException {
		// TODO Auto-generated method stub

		BufferedReader  entrada = new BufferedReader (new InputStreamReader (System.in));
		
		Pila p  = new Pila();
		
		String texto="Chocolate";
		Integer num=1001;
		
		Persona p1= new Persona ("Manolito",1.70f);
		Persona p2= new Persona ("Julio",1.73f);
		Persona p3= new Persona ("carlos",1.80f);
		Persona p4=new Persona ("Andres",1.90f);
		p.add(p1);
		p.add(p2);
		p.add(p3);
		
		System.out.println(p.toString());
		
		Naipe n1 = new Naipe(1,"Diamantes");
		Naipe n2 = new Naipe(2,"Corazones");
		Naipe n3 = new Naipe (3,"Pica");
		p.add(n1);
		p.add(n2);
		p.add(n3);
		
		System.out.println(p.toString());
		p.add(texto);
		p.add(num);
		System.out.println(p.toString());
		
		System.out.println(p.contiene(n3));
		
		System.out.println(p.eliminar(n3));
		
		System.out.println(p.contiene(n3));
	}

}
