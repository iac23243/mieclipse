package ej6;

public class Persona {

	protected String nombre;
	protected float altura;
	
	public Persona () {
		this.nombre="";
		this.altura=0;
	}
	
	public Persona(String nom, float alt) {
		this.nombre=nom;
		this.altura=alt;
	}

	public String getNombre() {
		return nombre;
	}

	public float getAltura() {
		return altura;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}

	
	public String toString() {
		return "Persona [nombre=" + nombre + ", altura=" + altura + "]";
	}
	
	
	
	
	
}
