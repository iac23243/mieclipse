package ej6;

public class Naipe {

	protected int numero;
	protected String palo;
	
	public Naipe() {
		this.numero=0;
		this.palo="";
	}
	
	public Naipe(int numero, String palo) {
		this.numero = numero;
		this.palo = palo;
	}

	public int getNumero() {
		return numero;
	}

	public String getPalo() {
		return palo;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public void setPalo(String palo) {
		this.palo = palo;
	}

	@Override
	public String toString() {
		return "Naipe [numero=" + numero + ", palo=" + palo + "]";
	}
	
	
	
}
