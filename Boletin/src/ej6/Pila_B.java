package ej6;

import java.util.LinkedList;

public class Pila_B <E> {

	private LinkedList <E> lista = new LinkedList<>();
	
	private E elemento;
	
	public Pila_B() {
		
	}
	
	public Pila_B (E element) {
		this.elemento=element;
	}

	public E getElemento() {
		return elemento;
	}

	public void setElemento(E elemento) {
		this.elemento = elemento;
	}
	
	public void add(E element) {
		lista.add(element);
	}


	public String toString() {
		return lista.toString();
	}
	
}
