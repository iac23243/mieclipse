package ej6;

import java.util.LinkedList;

public class Pila <T>{

	
	private LinkedList <T> lista = new LinkedList<>();
	
	private T elemento;
	
	public Pila() {
		
	}
	
	public Pila (T element) {
		this.elemento=element;
	}

	public T getElemento() {
		return elemento;
	}

	public void setElemento(T elemento) {
		this.elemento = elemento;
	}
	
	public void add(T element) {
		lista.add(element);
	}


	public String toString() {
		return lista.toString();
		
	}
	
	public Boolean contiene(T elemento) {
		return lista.contains(elemento);
	}
	
	public Boolean eliminar(T e) {
		return lista.remove(e);
	}
	
}
