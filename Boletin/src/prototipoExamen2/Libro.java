package prototipoExamen2;

import java.util.Comparator;
import java.util.Objects;

public class Libro implements Comparable <Libro>{

	private String titulo;
	private String autor;
	private Integer anyo;
	private Genero gen;
	
	public Libro () {
		this.titulo="";
		this.autor="";
		this.anyo=0;
		this.gen=Genero.FANTASIA;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(anyo, autor, gen, titulo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Libro other = (Libro) obj;
		return Objects.equals(anyo, other.anyo) && Objects.equals(autor, other.autor) && gen == other.gen
				&& Objects.equals(titulo, other.titulo);
	}

	public Libro(String titulo, String autor, int anyo, Genero gen) {
		super();
		this.titulo = titulo;
		this.autor = autor;
		this.anyo = anyo;
		this.gen = gen;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getAutor() {
		return autor;
	}

	public Integer getAnyo() {
		return anyo;
	}

	public Genero getGen() {
		return gen;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public void setAnyo(int anyo) {
		this.anyo = anyo;
	}

	public void setGen(Genero gen) {
		this.gen = gen;
	}

	public String toString() {
		return "Libro [titulo=" + titulo + ", autor=" + autor + ", anyo=" + anyo + ", gen=" + gen + "]";
	}

	public int compareTo(Libro o) {
	
		if(this.titulo==o.titulo) {
			if(this.autor==o.autor) {
				if(this.anyo==o.anyo) {
					if(this.gen==o.gen) {
						return 0;
					}else {
						return this.gen.compareTo(o.gen);
					}
				}else {
					return this.anyo.compareTo(o.anyo);
				}
			}else {
				return this.autor.compareTo(o.autor);
				
			}
		}else {
			return this.titulo.compareTo(o.titulo);
		}
		

	}

	public static class Comparador implements Comparator <Libro>{

		@Override
		public int compare(Libro o1, Libro o2) {
			return o1.compareTo(o2);
		}

	}//fin comparador
	
	public static class ComparadorAnyo implements Comparator <Libro>{

		@Override
		public int compare(Libro o1, Libro o2) {
			return o1.getAnyo().compareTo(o2.getAnyo());
		}

	}//fin comparadorAnyo
	
	public static class ComparadorTitulo implements Comparator <Libro>{

		@Override
		public int compare(Libro o1, Libro o2) {
			return o1.getTitulo().compareTo(o2.getTitulo());
		}

	}//fin comparadorTitulo"
	
	
	
	
}
