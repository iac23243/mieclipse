package ej8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayList <Integer> num=new ArrayList<>(Numeros.aleatorios());	
		System.out.println("Lista original");
		System.out.println(num.toString());
		
		System.out.println("Lista ordenada de menor a mayor");
		
		Collections.sort(num);
		
		System.out.println(num.toString());
		
		System.out.println("Ahora de mayor a menor");
		
		Collections.reverse(num);
		
		System.out.println(num.toString());
		
		
		System.out.println("-------------------------------");
		
		ArrayList <Personas> personas = new ArrayList<>();		
		Personas p1= new Personas("Luis",20);
		Personas p2 = new Personas("Manuel",40);
		Personas p3= new Personas("Maria",28);
		Personas p4= new Personas ("Jose",19);
		Personas p5=new Personas("Antonia",53);
		Personas p6=new Personas("Luis",21);
		
		personas.add(p6);
		personas.add(p5);
		personas.add(p4);
		personas.add(p3);
		personas.add(p2);
		personas.add(p1);
	
		System.out.println("Lista de personas");
		System.out.println(personas.toString());
		
		System.out.println("Lista ordenada de menor a mayor por edad y nombre");
		Collections.sort(personas,new Personas.Comparador());
		System.out.println(personas.toString());
		
		System.out.println("Lista de mayor a menor");
		
		Collections.reverse(personas);
		
		System.out.println(personas.toString());
		
	}

}
