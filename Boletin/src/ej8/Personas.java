package ej8;

import java.util.Comparator;

public class Personas implements Comparable<Personas> {

	private String nombre;
	private Integer edad;

	public Personas(String nombre, int edad) {
		super();
		this.nombre = nombre;
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public int compareTo(Personas o) {

		if (this.edad == o.edad) 
		{
			if (this.nombre == o.nombre) 
			{
				return 0;
			}
			else 
			{
				return this.nombre.compareTo(o.nombre);
			}
		}
		else
		{
			return this.edad.compareTo(o.edad);
		}
	}//fin compare
	
	
	
	
	@Override
	public String toString() {
		return "Personas [nombre=" + nombre + ", edad=" + edad + "]";
	}




	public static class Comparador implements Comparator <Personas>{

		public int compare(Personas o1, Personas o2) {
			// TODO Auto-generated method stub
			return o1.compareTo(o2);
		}
		
	}
	
	
	
	
	

}
