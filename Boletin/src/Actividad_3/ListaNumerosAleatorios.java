package Actividad_3;

import java.util.*;

public class ListaNumerosAleatorios {

	Random random = new Random();
	List<Integer> lista = new ArrayList<>();

	public void generarAleatorio() {

		for (int i = 0; i < 20; i++) {
			int aleatorio = random.nextInt(10) + 1;
			lista.add(aleatorio);
		}
		System.out.println("Lista de numeros aleatorios");
		System.out.println(lista);
	}

	public void sinRepetir() {
		Set<Integer> sinRepetir = new HashSet<>(lista);
		System.out.println("\nLista de numeros sin Repetir");
		System.out.println(sinRepetir);
	}

	public void repetidos() {
		Set<Integer> repetidos = new HashSet<>();
		int numTemp;
		for (int i = 1; i < lista.size(); i++) {
			numTemp = lista.get(i - 1);
			for (int j = i; j < lista.size(); j++) {
				// System.out.println(numTemp + "=" + lista.get(j));
				if (numTemp == lista.get(j)) {
					repetidos.add(numTemp);
				}
			}

		}

		System.out.println("\nLista de numeros Repetidos");
		System.out.println(repetidos);
	}

	public void unicos() {
		Set<Integer> unicos = new HashSet<>();
		int numTemp;
		int cont;
		for(int i = 0; i < lista.size(); i++) {
			numTemp = lista.get(i);
			cont=0;
			for (int j = 0; j < lista.size(); j++) {
				if(numTemp == lista.get(j)) {
					cont++;
				}
			}
			if(cont == 1) {
				unicos.add(numTemp);
			}
		}
		
		System.out.println("\nLista de numeros Unicos");
		System.out.println(unicos);
		
	}
	
	
	
}
