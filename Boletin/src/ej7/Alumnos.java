package ej7;

import java.util.Comparator;

public class Alumnos implements Comparable<Alumnos> {

	private String nombre;
	private Integer edad;
	private Float nota;

	public Alumnos() {
		this.nombre = "";
		this.edad = 0;
		this.nota = 0f;
	}

	public Alumnos(String nombre, int edad, Float nota) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.nota = nota;
	}

	public String getNombre() {
		return nombre;
	}

	public int getEdad() {
		return edad;
	}

	public Float getNota() {
		return nota;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public void setNota(Float nota) {
		this.nota = nota;
	}

	public String toString() {
		return "Alumnos [nombre=" + nombre + ", edad=" + edad + ", nota=" + nota + "]";
	}

	public int compareTo(Alumnos o) {
		if (this.nota == o.nota) {
			if (this.edad == o.edad) {
				if (this.nombre == o.nombre) {
					return 0;
				} else {
					return this.nombre.compareTo(o.nombre);
				}
			} else {
				return this.edad.compareTo(o.edad);
			}

		} else {
			return this.nota.compareTo(o.nota);
		}

	}
	
	/*public int compareToInverso(Alumnos o) {
		if (o.nota == this.nota) {
			if ( o.edad== this.edad) {
				if (o.nombre == this.nombre) {
					return 0;
				} else {
					return this.nombre.compareTo(o.nombre);
				}
			} else {
				return this.edad.compareTo(o.edad);
			}

		} else {
			return this.nota.compareTo(o.nota);
		}

	}*/
	
	
	
	public static class Comparar implements Comparator <Alumnos>{

		public int compare(Alumnos o1, Alumnos o2) {
			// TODO Auto-generated method stub
			return o1.compareTo(o2);
		}
		
		/*public int compareInverso(Alumnos a1, Alumnos a2) {
			return a1.compareToInverso(a2);
		}*/
		
	}
}
