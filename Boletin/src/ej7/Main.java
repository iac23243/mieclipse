package ej7;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main  {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));

		ArrayList<Alumnos> lista = new ArrayList<>();
		Alumnos a1 = new Alumnos("Ángela", 20, 90.0f);
		Alumnos a2 = new Alumnos("Bruno", 22, 90f);
		Alumnos a3 = new Alumnos("Carlos", 20, 99f);
		Alumnos a4 = new Alumnos("Daniela", 22, 100f);

		lista.add(a4);
		lista.add(a1);
		lista.add(a2);
		lista.add(a3);

		System.out.println("Lista original");
		System.out.println(lista.toString());
	
		Collections.sort(lista,new Alumnos.Comparar());
		System.out.println("Lista ordenada");
		System.out.println(lista.toString());
		
		Collections.sort(lista, new Alumnos.Comparar());
		System.out.println("Orden inverso");
		System.out.println(lista.toString());

	}

	/*public static class Comparador implements Comparator<Alumnos>{
		public int compare(Alumnos a1, Alumnos a2) {

			return a1.compareTo(a2);
		}
	}*/
}
