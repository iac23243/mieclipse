package Actividad_2;

import java.util.*;

public class NumerosEnteros {

	List<Integer> numEnteros = new ArrayList<>();
	List<Integer> numeroPares = new ArrayList<>();
	private static int rep = 0;

	public int añadirNum(int num) {
		if (num >= 0) {
			numEnteros.add(num);
			rep = 0;
		} else {
			rep = 1;
		}
		return rep;
	}

	public void numeroPares() {

		int repeticione = numEnteros.size();
		int temp;

		for (int i = 0; i < repeticione; i++) {
			temp = numEnteros.get(i);

			if (temp % 2 == 0) {
				numeroPares.add(temp * 100);
			}
		}

	}

	public void leerLista() {
		System.out.println(numeroPares);
	}
}
