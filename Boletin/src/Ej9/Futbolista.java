package Ej9;

public class Futbolista {
	
	
	private String nombre;
	private int dorsal;
	private String equipo;
	private Demarcacion demarcacion;
	

	public Futbolista (String name, int dorsal, String equipo, Demarcacion d) {
		this.nombre=name;
		this.dorsal=dorsal;
		this.equipo=equipo;
		this.demarcacion=d;
		
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public int getDorsal() {
		return dorsal;
	}
	
	
	public String getEquipo() {
		return equipo;
	}
	
	public Demarcacion getDemarcacion() {
		return demarcacion;
	}
	
	public String toString() {
		return "Nombre: "+nombre+" Dorsal: "+dorsal+" Equipo: "+equipo+" Demarcacion "+demarcacion;
	}
}
