package Ej9;

import java.util.ArrayList;


public class Equipo {

	private String nombre;
	private ArrayList<Futbolista> jugadores;
	
	public Equipo (String name) {
		this.nombre=name;
		this.jugadores=new ArrayList<Futbolista>();
	}
	
	
	public Boolean addFutbolista(Futbolista f) {
		return jugadores.add(f);
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public ArrayList<Futbolista> getJugadores(){
		return jugadores;
	}


	@Override
	public String toString() {
		return "Equipo [nombre=" + nombre + ", jugadores=" + jugadores.toString() + "]";
	}
	
	
	
}
