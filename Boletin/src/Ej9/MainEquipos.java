package Ej9;

public class MainEquipos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Demarcacion portero = Demarcacion.PORTERO;
		Demarcacion defensa =  Demarcacion.DEFENSA;
		Demarcacion delantero = Demarcacion.DELANTERO;
		Demarcacion  centro = Demarcacion.CENTROCAMPISTA;
		Equipo madrid = new Equipo("Real Madid");
		Equipo sevilla = new Equipo("Sevilla");
		Equipo betis = new Equipo("Betis");
		Equipo barsa = new Equipo("Barça");
		
		Futbolista courtois = new Futbolista("Courtois",1,"Real Madrid",portero);
		Futbolista navas = new Futbolista("Jesus Navas",16,"Sevilla",delantero);
		Futbolista joaquin = new Futbolista("Joaquin",17,"Betis",centro);
		Futbolista pique = new Futbolista("Pique",6,"Barça",defensa);
		
		madrid.addFutbolista(courtois);
		sevilla.addFutbolista(navas);
		betis.addFutbolista(joaquin);
		barsa.addFutbolista(pique);
		
		
		System.out.println("Los futbolistas son:");
		System.out.println(madrid.toString());
		System.out.println(sevilla.toString());
		System.out.println(betis.toString());
		System.out.println(barsa.toString());
	}

}
