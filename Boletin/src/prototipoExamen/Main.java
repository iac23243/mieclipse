package prototipoExamen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.TreeSet;

public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));

		ArrayList<Empleado> li = new ArrayList<Empleado>();

		/*
		 * Departamento rh = Departamento.RECURSOS_HUMANOS; Departamento mant =
		 * Departamento.MANTENIMIENTO; Departamento des = Departamento.DESARROLLO;
		 * Departamento mark = Departamento.MARKETING;
		 * 
		 * 
		 * 
		 * Empleado e1 = new Empleado("Miguel",26,des); Empleado e2 = new Empleado
		 * ("Alberto",24,des); Empleado e3= new Empleado ("Manolo",26,mant); Empleado e4
		 * = new Empleado ("Venancio",28,rh);
		 * 
		 * li.add(e3); li.add(e2); li.add(e1); li.add(e4);
		 * 
		 * System.out.println("Lista original"); System.out.println(li.toString());
		 * 
		 * Collections.sort(li, new Empleado.Comparador());
		 * 
		 * System.out.println("Lista ordenada"); System.out.println(li.toString());
		 */

		Departamento dep = null;
		int edad = 0, menu = 0;
		boolean control = true, bucle = true;
		String nombre;

		do {

			System.out.println("Introduzca el nombre del nuevo empleado");
			nombre = entrada.readLine();

			do {
				try {

					System.out.println("¿Cuántos años tiene?");
					edad = Integer.parseInt(entrada.readLine());
					control = true;
				} catch (NumberFormatException e) {
					System.out.println("caracter inválido");
					control = false;
				}
			} while (!control);
			System.out.println("¿A qué departamento pertenece?");
			do {

				try {

					System.out.println("Pulse:" + "\n1. Recursos humanos" + "\n2. Mantenimiento" + "\n3. Desarrollo"
							+ "\n4. Marketing");
					menu = Integer.parseInt(entrada.readLine());
					control = true;
				} catch (NumberFormatException e) {
					System.out.println("caracter inválido");
					control = false;
				}
			} while (!control);
			switch (menu) {
			case 1:
				dep = Departamento.RECURSOS_HUMANOS;

				break;

			case 2:
				dep = Departamento.MANTENIMIENTO;
				break;
			case 3:
				dep = Departamento.DESARROLLO;
				break;

			case 4:
				dep = Departamento.MARKETING;
				break;

			}

			li.add(new Empleado(nombre, edad, dep));

			System.out.println("Si desea añadir otro pulse 1, si no pulse 2");
			menu = Integer.parseInt(entrada.readLine());

			if (menu == 2) {
				bucle = false;
			}

		} while (bucle);

		System.out.println("La lista original es la siguiente");
		System.out.println(li.toString());
		
		System.out.println("La lista ordenada es la siguiente");
		
		Collections.sort(li, new Empleado.Comparador());
		
		System.out.println(li.toString());
		
		HashSet<Empleado> hash = new HashSet<>(li);
		
		System.out.println(hash.toString());
		
		TreeSet<Empleado> tree = new TreeSet<>(hash);
		
		//Collections.sort
	}

}
