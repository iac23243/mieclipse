package prototipoExamen;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;

public class Empleado implements Comparable <Empleado>{
	
	//ArrayList <Empleado> lista = new ArrayList<>();
	private String nombre;
	private Integer edad;
	private Departamento dep;
	private int codigo;

	public Empleado(String nombre, int edad, Departamento dep) {
		this.nombre = nombre;
		this.edad = edad;
		this.dep = dep;
	}
	
	public Empleado() {
		this.nombre = "";
		this.edad = 0;
		this.dep = null;
	}

	public String getNombre() {
		return nombre;
	}

	public int getEdad() {
		return edad;
	}

	public Departamento getDep() {
		return dep;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public void setDep(Departamento dep) {
		this.dep = dep;
	}

	

	@Override
	public int hashCode() {
		return Objects.hash(dep, edad, nombre);
	}

	

	public String toString() {
		return "Empleado [nombre=" + nombre + ", edad=" + edad + ", dep=" + dep + "]";
	}

	public int compareTo(Empleado e) {
	
		if(this.edad==e.edad) {
			if(this.nombre==e.nombre) {
				return 0;
			}else {
				return this.nombre.compareTo(e.nombre);
			}
		}else {
			return this.edad.compareTo(e.edad);
		}
		
	}
	
	
	
	/*public Boolean addEmpleado(Empleado e) {
		return lista.add(e);
	}*/

	public static class Comparador implements Comparator <Empleado>{

		@Override
		public int compare(Empleado o1, Empleado o2) {
			return o1.compareTo(o2);
		}
		
		public int compareNombre(Empleado o1, Empleado o2) {
			return o1.nombre.compareTo(o2.nombre);
		}
	}



}


