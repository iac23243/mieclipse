package ej4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainSorteo {

	public static void main(String[] args) throws NumberFormatException, IOException {
		// TODO Auto-generated method stub

		BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));

		Sorteo sorteo = new Sorteo();
		int num, cont = 0;
		boolean control = false;
		String nombre;
		for (int i = 0; i < 10; i++) {

			do {

				try {
					System.out.println("Introduzca numeros para hacer un sorteo. Número " + cont + " de 10");
					//num = Integer.parseInt(entrada.readLine());
					nombre=entrada.readLine();
					control=true;
					sorteo.add(nombre);
					
				} catch (NumberFormatException e) {
					System.out.println("Carácter inválido. Pruebe otra vez");
					control = false;
				} // fin try catch

			} while (!control);
			cont++;
		}

		System.out.println("Los números introducidos son");
		System.out.println(sorteo.toString());
		
		System.out.println("¿Cuantos números deseas premiar?");
		num=Integer.parseInt(entrada.readLine());
		
		sorteo.premiados(num);
		
	}

}
