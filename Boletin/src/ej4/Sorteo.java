package ej4;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

public class Sorteo<T> {

	private Set<T> coleccion = new HashSet<>();

	public Sorteo() {

	}
	
	public Boolean add(T elemento) {
		if (coleccion.add(elemento)) {
			System.out.println("Elemento añadido");
			return true;
		} else {
			System.out.println("Este elemento ya estaba en la coleccion");
			return false;
		}
	}

	public Set <T> premiados (int numPremiados){
		Set <T>premiados=new HashSet<>();
		Set <T> invalido = null;
		Set <Integer> numeros =  new  HashSet<>();
		Random r = new Random();
		ArrayList <T> copia =  new ArrayList<>(coleccion);
		int aux=0;
		
		if(numPremiados>=coleccion.size()){
			System.out.println("Cantidad no válida");
			return invalido;
		}else {
		
			for(int i=0;i<=numPremiados;i++) {
			aux=r.nextInt(coleccion.size());
			numeros.add(aux);
			
			}//fin for
			
		
		Collections.shuffle(copia);
		Iterator <Integer> it = numeros.iterator();
		
		while(it.hasNext()) {
			aux=it.next();
			premiados.add(copia.get(aux));
		}
		
		/*for(int i=0;i<copia.size();i++) {
			//premiados.add(copia.get())
		}*/
			System.out.println("Los números premiados son: "+premiados.toString());
			return premiados;
		}//fin de else
		
		
		
	}

	
	public String toString() {
		return coleccion.toString();
	}

}
