package hashMap;

/*Vamos a considerar que una Persona será igual a otra Persona si tienen el mismo id en este caso
para facilitar el ejemplo. Y para el caso de orden, vamos a considerar que una Persona es mayor
que otra si su id es mayor.*/

public class Persona {
	
	public int idPersona;
	public String nombre;
	public int altura;
	
	public Persona(int idPersona, String nombre, int altura)
	{ 
		this.idPersona = idPersona; this.nombre = nombre; this.altura=altura;}
	

	@Override
	public String toString() 
	{ 
	return "Persona-> ID: "+idPersona+" Nombre: "+nombre + "Altura: "+altura+"\n"; 
		}
	
	@Override
	public boolean equals(Object obj) 
	{
		if (obj == null) 
		{ 
			return false; 
		}
		if (getClass() != obj.getClass()) 
		{ 
			return false; 
		}
		final Persona other = (Persona) obj;
		if (this.idPersona != other.idPersona) 
		{ 
			return false; 
		}
	return true;
	}
} 