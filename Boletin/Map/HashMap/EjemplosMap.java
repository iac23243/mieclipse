package hashMap;



import java.util.Collection;
import java.util.Collections;

/*Ahora crearemos un programa que contendrá un mapa al cual añadiremos pares <clave,valor>
del tipo <String, Double> . El String será la Clave y el Double será el Valor*/

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
public class EjemplosMap {
	public static void main (String []args) {
		
		Map <String,Double> m = new HashMap<>();
				
		m.put("Ana",1.65);
		m.put("Marta",1.60);
		m.put("Luis",1.73);
		m.put("Pedro",1.71);
		m.put("Lucas",1.8);
		m.put("Jorge",1.75);

		
		System.out.println("Personas en el mapa: \n"+m.toString().replaceAll(",", "\n"));
		
		System.out.println("\nProbrando métodos de HashMap\n");
		

		System.out.println("Distintas Formas de crear una Vista de un Mapa\n ");
	
        // Usando keySet() para tener un set de las claves del Map
        System.out.println("\nUtilizando el método \"Set<K>keySet()\"");
        System.out.println( "El set es: " + m.keySet());
        
        //Devuelve una vista Collection de los valores del Map
        System.out.println("\nCon el metodo \"Collection values()\"");
		System.out.println("La coleccion es:\n "+m.values());
		
		//Metodo pra obtener una vista de las entradas
		/*La interfaz Map.Entry en Java proporciona ciertos métodos para acceder a la entrada en el mapa. 
		 * Al acceder a la entrada del Mapa, podemos manipularlos fácilmente. 
		 * Map.Entry es genérico y está definido en el paquete java.util.*/
		Set<Map.Entry<String, Double>> entradas = m.entrySet();
		System.out.println("\nUtilizando \"Map.Entry<>\"");
		System.out.println(entradas);
		
		System.out.println("\nVeamos otros metodos de Map");
        System.out.println("¿Esta presente el valor 456?: "+m.containsValue(456));
		System.out.println("¿Esta presente el valor 1.60?: "+m.containsValue(1.60));
		System.out.println("¿Esta presente la clave Marta?: "+m.containsKey("Marta"));
		System.out.println("¿Esta presente la clave Hola?: "+m.containsKey("Hola"));

		// Mostando the HashMap
        System.out.println("\nEl Mapa inicial es: " + m);
  
        //iteramos el HashMap de una de las formas posibles: utilizando keySet()
        System.out.println("\nrealizamos un forEach y un keySet para iterar");
    	for (String key : m.keySet()) {
		     System.out.println(key +" : "+m.get(key));
		}
        
    	System.out.println("\n\nVamos a iterar el Map por ejemplo con Map.Entry y eliminaremos entradas segun una condicion: altura mayor a 171");
		//Metodo pra obtener una vista de las entradas
		Set<Map.Entry<String, Double>> entradasMap = m.entrySet(); //Set de entradas del Map
		Iterator<Map.Entry<String, Double>> it ; //iterador de entradas
		//bucle del iterator realizado con for
/*
		for (it=entradasMap.iterator();it.hasNext();) {
			Map.Entry<String, Double> e = it.next();
			if (e.getValue()>1.71) //getvalue() es un metodo de la interface Map.Entry que devuelve el valor
			{
				it.remove();
			}
		}
		*/
		//iterator realizado con while
		it=entradasMap.iterator();
		while (it.hasNext() ) 
		{
			Map.Entry<String, Double> e = it.next();			
			if (e.getValue()>1.71) 
			{
				it.remove(); 
			} 
		}
		System.out.println(m);
    	
		System.out.println("\nEl ejemplo anterior se podría haber hecho tambien actuando sobre la vista de valores como la ejecutada un poco mas arriba");
        
		//Devuelve una vista Collection de los valores del Map
		Collection<Double> estaturas= m.values();
		
		Iterator <Double> it2; //iterador de valores
		
/*		//bucle del iterator realizado con while
		it2= estaturas.iterator();
		while (it2.hasNext() ) 
		{
			Double v = it2.next();

			if (v>1.61) 
			{
				it2.remove(); 
			} 
		}
		System.out.println(m);

		*/
		//iterator realizado con while

		for (it2= estaturas.iterator();it2.hasNext();) {
			Double v = it2.next();
			if (v>1.61) 
			{
				it2.remove();
			}
		}	
		
		System.out.println(m);
		

		//Mostramos las entradas una a una utilizando un forEach
	    //Se puede omitir el operador diamante y el tipo dentro de él
//	    for (Map.Entry<String,Double> ejemplo: m.entrySet()) {
	    System.out.println("\nEntradas del diccionario extraídas una a una:");
	    for (Map.Entry ejemplo: m.entrySet()) {

	      System.out.println(ejemplo);
	    } 
	}//fin del Main
} //Fin de la clase