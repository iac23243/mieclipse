package hashMap;

import java.util.Collection;
import java.util.Collections;

/*Ahora crearemos un programa que contendrá un mapa al cual añadiremos pares <clave,valor>
del tipo <Integer, Persona> en el que usaremos un número como clave para cada Persona.*/

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
public class Programa {
	public static void main (String []args) {
		
		Map <Integer,Persona> mp = new HashMap<Integer,Persona>();
		
		Persona p; 
		
		p= new Persona(4,"María",167);
		mp.put(4, p); // Añadimos un objeto persona al map
		
		p = new Persona(1,"Marta",165);
		mp.put(1, p); // Añadimos un objeto persona al map
		
		p = new Persona(3,"Elena",185);
		mp.put(3, p); // Añadimos un objeto persona al map
		
		p = new Persona(2,"Yolanda",176);
		mp.put(2, p); // Añadimos un objeto persona al map
		
		p = new Persona(5,"María Dolores",169);
		mp.put(4, p); // Esto crea una colisión ¡Dos objetos no pueden tener la misma clave!
		
		System.out.println("Personas en el mapa: \n"+mp.toString().replaceAll(",", "\n"));
		
		System.out.println("Probrando métodos de HashMap");
		

		System.out.println("Distintas Formas de crear una Vista de un Mapa ");
	
        // Usando keySet() para tener un set de las claves del Map
        System.out.println("Utilizando el método \"Set<K>keySet()\"");
        System.out.println( "El set es: " + mp.keySet());
        
        //Devuelve una vista Collection de los valores del Map
        System.out.println("Con el metodo \"Collection values()\"");
		System.out.println("La coleccion es:\n "+mp.values());
		
		//Metodo pra obtener una vista de las entradas
		Set <Map.Entry<Integer, Persona>> entradas = mp.entrySet();
		System.out.println("Utilizando \"Map.Entry<>\"");
		System.out.println(entradas);
		
		System.out.println("Veamos otros metodos de Map");
        System.out.println("¿Esta presente el valor Remedios?: "+mp.containsValue("Remedios"));
		System.out.println("¿Esta presente el valor Maria?: "+mp.containsValue("Maria"));
		System.out.println("¿Esta presente la clave 4?: "+mp.containsKey(4));
		System.out.println("¿Esta presente la clave 12?: "+mp.containsKey(12));

		// Mostando the HashMap
        System.out.println("El Mapa inicial es: " + mp);
  
        //iteramos el HashMap de una de las formas posibles: utilizando keySet()
        System.out.println("\nrealizamos un forEach y un keySet para iterar");
    	for (Integer key : mp.keySet()) {
		     System.out.println(key +" : "+mp.get(key));
		}
        
    	System.out.println("Vamos a iterar el Map por ejemplo con Map.Entry y eliinaremos entradas segun una condicion");
		//Metodo para obtener una vista de las entradas
		Set <Map.Entry<Integer, Persona>> entradasMap = mp.entrySet(); //Set de entradas del Map
		Iterator <Map.Entry<Integer, Persona>> it; //iterador de entradas
		for (it=entradas.iterator();it.hasNext();) {
			Map.Entry<Integer, Persona> e = it.next();
			if (e.getValue().altura>175) 
			{
				it.remove();
			}
		}
		System.out.println(mp);
    	
		System.out.println("El ejemplo anterior se podría haber hecho tambien actuando sobre la vista de valores como la ejecutada un poco mas arriba");
        //Devuelve una vista Collection de los valores del Map
		//NO FUNCIONA
		Collection <Persona> person= mp.values();
		
		Iterator <Persona> it2; //iterador de valores
		for (it2= person.iterator();it.hasNext();) {
			Persona persona = it2.next();
			
			if (persona.altura>160) 
			{
				it2.remove();
			}
		}	
		System.out.println(mp);
		}


} 