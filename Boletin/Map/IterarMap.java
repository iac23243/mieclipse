package hashMap;

/*Map es una interfaz que se utiliza para recoger datos en forma de par llave-valor. 
 * Java provee varias formas de iterar elementos del mapa como for bucle, for-each bucle, while bucle, método forEach(), etc. 
 * Veamos los ejemplos.*/

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class IterarMap{
	public static void main(String[] args){
		Map<Integer, String> map = new HashMap<>();
		map.put(10, "Ten");
		map.put(100, "Hundred");
		map.put(1000, "Thousand");

		 System.out.println("\nEJEMPLO 1: Iterar los elementos del Map usando for bucle en Java");

		 /* Usamos un simple for bucle para iterar los elementos del Map. 
		 * Aquí, en el bucle se usa el método iterator() para obtener entradas.*/
		
		for (Iterator<Map.Entry<Integer, String>> entries = map.entrySet().iterator(); entries.hasNext(); ) 
		{
		    Map.Entry<Integer, String> entry = entries.next();
		    System.out.println(entry.getKey()+" : "+entry.getValue());
		}
		
		System.out.println("\nEJEMPLO 2: Iterar los elementos Map usando foreach en Java");
		  
		 /* Utilizamos el bucle for-each y el método entrySet() para iterar cada entrada del mapa. 
		 * El entrySet() devuelve un conjunto de entradas del mapa.
		 * */
		
		for (Map.Entry<Integer, String> entry : map.entrySet()) {
		    System.out.println(entry.getKey() + ":" + entry.getValue());
		}
		
		
		  System.out.println("\nEJEMPLO 3: Iterar los elementos del Map usando Entry e Iterator en Java");
		  
		 /* El método iterator() devuelve un Iterator para atravesar los elementos mientras que Entry se usa para recoger la entrada de Map.
		 * */
		
		Iterator<Map.Entry<Integer, String>> entries = map.entrySet().iterator();
		while (entries.hasNext()) {
			Map.Entry<Integer, String> entry = entries.next();
			System.out.println(entry.getKey() + ":" + entry.getValue());
		}
		
		System.out.println("\nEJEMPLO 4: Iterar los elementos del Map usando for-each y keySet() en Java");
		
		/*El método keySet() se usa para recoger un conjunto de claves de Map que luego se usa para iterar usando un bucle for-each.*/
		
		for (Integer key : map.keySet()) {
		     System.out.println(key +" : "+map.get(key));
		}
		
		System.out.println("\nEJEMPLO 5: Iterar los elementos del Map usando el while bucle en Java");
		/*Aquí, usamos el método iterator() para obtener el iterador de claves y luego iterar estas claves usando el bucle while. 
		 * Para obtener el valor de una clave, usamos el método get()*/
		
		Iterator<Integer> itr = map.keySet().iterator();
		while (itr.hasNext()) {
		    Integer key = itr.next();
		    System.out.println(key +" : "+map.get(key));
		}
		
		System.out.println("\nEJEMPLO 6: Iterando elementos del Map usando Stream y forEach en Java");
		/*Podemos usar la corriente para iterar los elementos. 
		 * Aquí, usamos entrySet() para recoger las entradas del mapa que se desplazaron a través del método forEach().*/
		
		map.entrySet()
		.stream()
		.forEach(System.out::println);
		
		System.out.println("\nEJEMPLO 7: Iterar los elementos del mapa usando forEach y lambda en Java");
		
		/*También podemos usar la expresión lambda para iterar los elementos del mapa. Aquí, usamos la expresión lambda dentro del método forEach().*/
		
		map.forEach((key, value) -> System.out.println(key + " : " + value));

	}
}