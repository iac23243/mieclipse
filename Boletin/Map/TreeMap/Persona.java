package sortedMap;

import java.util.SortedMap;
import java.util.TreeMap;

/*Hemos modificado la clase persona  con respecto al ejemplo con HashMap, añadiendo un campo agendatel que es un mapa que nos
simulará la agenda de teléfonos. La hacemos pública para poder acceder a ella desde nuestra
clase Programa. Hemos redefinido el método toString para que se muestre la agenda también. */

public class Persona {
	public int idPersona;
	public String nombre;
	public int altura;
	
	public SortedMap<String,String> agendatel;
	
	public Persona(int idPersona, String nombre, int altura) {
		this.idPersona = idPersona; this.nombre = nombre; this.altura = altura;
		this.agendatel = new TreeMap<String,String>(); //inicialmente el mapa está vacío
	}
	
	@Override
	public String toString() {
		return "Persona-> ID: "+idPersona+" Nombre: "+nombre +" Altura: "+altura+"\nAgenda:\n"+agendatel.toString().replaceAll(",","\n");
	}
	
	

	
	public SortedMap<String, String> getAgendatel() {
		return agendatel;
	}

	public void setAgendatel(SortedMap<String, String> agendatel) {
		this.agendatel = agendatel;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) 
		{ 
			return false; 
		}
		if (getClass() != obj.getClass()) 
		{ 
			return false; 
		}
		final Persona other = (Persona) obj;
		if (this.idPersona != other.idPersona) 
		{ 
			return false; 
		}
	return true;
	}
}