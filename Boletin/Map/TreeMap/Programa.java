package sortedMap;

import java.util.Collection;

public class Programa {
	public static void main (String []args) {
		
		Persona p = new Persona(1,"María",167);
		
		p.agendatel.put("Trabajo", "954825748");
		p.agendatel.put("Oficina", "958746362");
		p.agendatel.put("Móvil","666555444");
		p.agendatel.put("Casa","954473456");
		p.agendatel.put("Casa","954473423");

		Persona p1 = new Persona(2,"Juan",175);
		
		p1.agendatel.put("Zapateria (Trabajo)", "954825748");
		p1.agendatel.put("Casa", "958746362");
		p1.agendatel.put("Móvil","666555444");
		p1.agendatel.put("Playa","954473456");
				
		System.out.println("\nPersonas en el mapa: \n"+p+"\n\n"+p1);
		
		System.out.println();
		
		/*El método equals de String implica que los objetos se ordenan por orden alfabético. Si
		quisiéramos acceder a un objeto de la agenda de teléfonos, por ejemplo al móvil de una persona,
		bastaría tan solo la instrucción: p.agendatel.get("Móvil"); donde “Móvil” es la clave que nos
		permite acceder a un objeto.*/
		
		System.out.println("El movil de "+p.nombre+" es "+p.agendatel.get("Móvil"));
		System.out.println("El telefono del trabajo de "+p1.nombre+" es "+p1.agendatel.get("Zapateria (Trabajo)"));


		
	}
}
