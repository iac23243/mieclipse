package arrayList;


import java.util.ArrayList;
import java.util.Iterator;

public class EjemploArrayList {

   public static void main(String[] args) {
       
        /*Creamos una lista enlazada con ArrayList de String, por ejemplo. Recuerda usar <Clase a manejar>.
         * ArrayList<String> listaMensajes=new ArrayList<String>();
         *
         * Desde Java 7, no es necesario indicar la clase la segunda vez, por lo que lo siguiente es igual al ejemplo anterior:*/
       ArrayList<Integer> lista = new ArrayList<>();
        
       //Metodo add. añade elementos a nuestra lista
       lista.add(10);
       lista.add(11);
       lista.add(12);
        
       System.out.println("ADD");
       System.out.println(lista.toString());
        
       //Metodo remove, elimina elementos de nuestra lista mediente indice
       lista.remove(2); //Elimino el ultimo elemento, no el elemento 2 ¡¡OJO CON ESO!!!
        
       System.out.println("\nREMOVE");
       System.out.println(lista.toString());
        
       //Metodo size, indica el numero de elementos de la lista
        
       System.out.println("\nSIZE");
       System.out.println("Numero de elementos de la lista: "+lista.size());
        
       //Metodo get, devolvemos un elemento de un indice
        
       System.out.println("\nGET");
       System.out.println("El valor del primer elemento de la lista es: "+lista.get(0)); //Primer elemento
        
       //Metodo Iterator, util para recorrer un arrayList
        
       System.out.println("\nITERATOR");
       Iterator<Integer> it = lista.iterator();
       int num;
       
       while (it.hasNext()){
           num = it.next();
           System.out.println(num);
            
       }
        
       //Metodo indexOf, util para saber la posicion de un elemento
        
       System.out.println("\nINDEXOF");
       System.out.println(lista.indexOf(10));
        
       //Metodo Clear, elimina todos los elementos
       lista.clear();
        
       System.out.println("\nCLEAR");
       System.out.println(lista.toString());
        
       //Metodo isEmpty, indica si un arraylist esta vacio o no
        
       System.out.println("\nisEmpty");
       System.out.println("¿El ArrayList \"Lista\" está vacío?: "+lista.isEmpty());
   }
    
}