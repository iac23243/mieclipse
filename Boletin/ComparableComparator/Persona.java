package comparableComparator;

import java.util.Comparator;
import java.util.Date;
class Persona implements Comparable<Persona> {


    private int idPersona;
    private String nombre;
    private java.util.Date fechaNacimiento;


    public Persona() {
    }


    public Persona(int idPersona, String nombre) {
        this.idPersona = idPersona;
        this.nombre = nombre;


    }


    public int getIdPersona() {
        return idPersona;
    }


    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }


    public String getNombre() {
        return nombre;
    }


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }


    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }


    @Override
    public int compareTo(Persona o) {
        return this.nombre.compareTo(o.nombre);
    }


    @Override
    public String toString() {
        return String.format("persona{idPersona:%1s,nombre:%2s}", idPersona, nombre);
    }
}


class OrdenarPersonaPorId implements Comparator<Persona> {


    @Override
    public int compare(Persona o1, Persona o2) {
        return o1.getIdPersona() - o2.getIdPersona();
    }
}
