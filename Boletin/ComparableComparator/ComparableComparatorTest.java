package comparableComparator;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


/**
*
* @author DSILVA
*/
public class ComparableComparatorTest {


   /**
    * @param args the command line arguments
    */
   public static void main(String[] args) {


       List<String> nombres = Arrays.asList("Carlos", "Ana", "Dionisio", "Bernardo");
       System.out.println("lista original:" + nombres);


       Collections.sort(nombres);
       System.out.println("lista ordenada:" + nombres);


       Set<String> otrosNombres = new TreeSet<>();
       otrosNombres.add("Mario");
       otrosNombres.add("Fernando");
       otrosNombres.add("Omar");
       otrosNombres.add("Juana");


       System.out.println("\nconjunto (TreeSet) ordenado:" + otrosNombres);


       Set<Persona> personas = new TreeSet<>();
       personas.add(new Persona(1, "Mario"));
       personas.add(new Persona(2, "Fernando"));
       personas.add(new Persona(3, "Omar"));
       personas.add(new Persona(4, "Juana"));


       System.out.println("conjunto ordenado (TreeSet) de personas: " + personas);


       List<Persona> otrasPersonas = Arrays.asList(new Persona(4, "Juana"),
               new Persona(2, "Fernando"),
               new Persona(1, "Mario"),
               new Persona(3, "Omar"));
       
       /*Para utilizar este comparador, debemos usar el parámetro adicional 
        * de Collections.sort(). 
        */
       Collections.sort(otrasPersonas, new OrdenarPersonaPorId());
       System.out.println("\nUtilizando el parametro de Collection.sort()");
       System.out.println("lista de personas ordenadas por ID:" + otrasPersonas);



       // ... o el parámetro del constructor de java.util.TreeSet. 
       Set<Persona> conjuntoPersonas = new TreeSet<>(new OrdenarPersonaPorId());
       conjuntoPersonas.add(new Persona(3, "Omar"));
       conjuntoPersonas.add(new Persona(4, "Juana"));
       conjuntoPersonas.add(new Persona(2, "Fernando"));
       conjuntoPersonas.add(new Persona(1, "Mario"));

       System.out.println("\nLo mismo pero utilizando el parámetro del constructor de java.util.TreeSet. ");
       System.out.println("conjunto de personas ordenadas por ID:" + conjuntoPersonas);
   }
}
